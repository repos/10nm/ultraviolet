import path from "path";
import fs from "fs";
import * as constants from "../../src/data/UltravioletConstants";
import { rootDir } from "../UltravioletWebTestUtils";

describe("Ultraviolet constants file tests", () => {
    const constantsFile = fs.readFileSync(
        path.resolve(rootDir, "src", "data", "UltravioletConstants.ts"),
        "utf8"
    );

    test("Ensure no other imports", () => {
        expect(constantsFile).not.toMatch(/^\s*import (?!buildinfo)/gim);
    });

    test("Internal version follows semantic versioning", () => {
        expect(constants.APP_VERSION).toMatch(/^\d*\.\d*\.\d*$/);
    });
});
