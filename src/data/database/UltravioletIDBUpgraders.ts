import UltravioletIDB, {
    UltravioletIDBUpgradeHandler,
} from "app/data/database/UltravioletIDB";

/**
 * A set of functions responsible for setting up the Ultraviolet IndexedDB
 * database. This object is indexed by its old version, with `0` being
 * the initial creation of all database objects. This means the handler
 * with an index of `1` is responsible for upgrading the database from
 * version `1` to `2`, `2` upgrades to `3`, and so on.
 */
export default <{ [key: number]: UltravioletIDBUpgradeHandler }>{
    0: (openRequest) => {
        const database = openRequest.result;

        // Creates the dependency cache
        UltravioletIDB.createObjectStore(database, "cacheTracker", "id", [
            "timestamp",
        ]);
        UltravioletIDB.createObjectStore(database, "dependencyCache", "id", [
            "lastCache",
            "etag",
            "data",
        ]);
        UltravioletIDB.createObjectStore(database, "groupCache", "name", [
            "page",
            "displayName",
        ]);
        UltravioletIDB.createObjectStore(database, "watchedPages", "title", []);
        UltravioletIDB.createObjectStore(database, "recentPages", "title", []);

        // Logging
        UltravioletIDB.createObjectStore(database, "errorLog", "id", [
            "timestamp",
            "code",
            "data",
        ]);
    },
};
