// import {Revert} from "app/mediawiki";
// import DiffViewerInjector from "app/ui/injectors/DiffViewerInjector";

import Log from "app/data/AppLog";
import { Page, RevertContextBase, Revision, User } from "app/mediawiki";
import UIInjectors from "app/ui/injectors/UIInjectors";

// https://meta.wikimedia.org/wiki/User:Krinkle/Tools/Real-Time_Recent_Changes
export default class RTRC {
    private static onRTRCPage: boolean;
    static init(): void {
        // https://github.com/Krinkle/mw-gadget-rtrc/blob/f84d47fa8e776d31ad6f83764930a352a695d572/src/rtrc.js#L1655-L1656
        RTRC.onRTRCPage =
            (mw.config.get("wgTitle") === "Krinkle/RTRC" &&
                mw.config.get("wgAction") === "view") ||
            (mw.config.get("wgCanonicalSpecialPageName") === "Blankpage" &&
                mw.config.get("wgTitle").split("/", 2)[1] === "RTRC");
        if (RTRC.onRTRCPage) {
            mw.hook("wikipage.diff").add(async (diff: JQuery) => {
                const newRevId = diff
                    .find("#mw-diff-ntitle2 > a[data-mw-revid]")
                    .data("mw-revid");
                const oldRevId = diff
                    .find("#mw-diff-otitle2 > a[data-mw-revid]")
                    .data("mw-revid");
                const page = Page.fromTitle(
                    diff.find("strong > a").attr("title")
                );
                const user = User.fromUsername(
                    diff.find("#mw-diff-ntitle2 > a > bdi").text()
                );

                const newRevision = Revision.fromID(newRevId, {
                    page,
                    user,
                });
                if (!newRevision.isPopulated()) await newRevision.populate();

                const oldRevision = Revision.fromID(oldRevId, {
                    page,
                    user,
                });

                const context: RevertContextBase = {
                    newRevision,
                    oldRevision,
                    latestRevision: await page.getLatestRevision(),
                };

                Log.trace("RTRC new diff", {
                    diff,
                    newRevId,
                    oldRevId,
                    page,
                    user,
                    newRevision,
                    oldRevision,
                });

                await UIInjectors.i.diffViewerInjector.loadOptions(
                    context,
                    false
                );
            });
        }
    }

    // TODO: post-revert action
    static nextDiff(): void {
        if (
            RTRC.onRTRCPage &&
            ($('input[name="autoDiff"]')[0] as HTMLInputElement).checked
        ) {
            $("#diffNext").trigger("click");
        }
    }
}
