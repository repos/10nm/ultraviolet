export enum RevertMethod {
    Unset,
    Rollback,
    Undo,
}
