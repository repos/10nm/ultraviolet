import { ClientUser, Page, WarningManager } from "app/mediawiki";
import {
    APP_FALLBACK_CONFIG,
    APP_WIKI_CONFIGURATION_PAGES,
    APP_WIKI_CONFIGURATION_VERSION,
} from "app/data/UltravioletConstants";
import Log from "app/data/AppLog";
import WikiConfiguration from "./WikiConfiguration";
import WikiConfigurationRaw from "./WikiConfigurationRaw";
import updateWikiConfiguration from "app/config/wiki/updateWikiConfiguration";
import WikiConfigurationDeserializers from "app/config/wiki/WikiConfigurationDeserializers";
import i18next from "i18next";
import UltravioletUI from "app/ui/UltravioletUI";
import MediaWikiNotificationContent from "app/ui/MediaWikiNotificationContent";
import UltravioletHooks from "app/event/UltravioletHooks";
import { UVUIToast } from "app/ui/elements/UVUIToast";

/**
 * This class handles every single contact with the Ultraviolet per-wiki
 * configuration file, usually found at `Project:Ultraviolet/configuration.json`.
 *
 * The path of the configuration file is modified with the constant
 * {@link APP_WIKI_CONFIGURATION_PAGES}.
 */
export default class UltravioletWikiConfiguration {
    public static readonly settingsKey = "uv-config-wiki";

    private static _loadedConfiguration: WikiConfiguration;
    public static get c(): WikiConfiguration {
        return UltravioletWikiConfiguration._loadedConfiguration;
    }

    static async loadWikiConfigurationFile(): Promise<Record<string, any>> {
        let preloadedData: Record<string, any>;
        try {
            const configurationPages = APP_WIKI_CONFIGURATION_PAGES.map((t) =>
                Page.fromTitle(t)
            );
            // Allow for custom wiki configuration overrides in debug mode.
            if (process.env.NODE_ENV !== "production") {
                configurationPages.splice(
                    0,
                    0,
                    Page.fromTitle(
                        `User:${ClientUser.i.username}/ultravioletWikiConfiguration.json`
                    )
                );
            }
            Log.debug("Loading on-wiki configuration file...");
            await Page.getLatestRevisions(configurationPages, {
                followRedirects: true,
                throwIfMissing: false,
            });
            const primaryConfiguration = configurationPages.find(
                (p) => p.latestCachedRevision != null
            );
            Log.debug(
                `Using configuration from ${primaryConfiguration.title.getPrefixedDb()}`
            );

            preloadedData = JSON.parse(
                primaryConfiguration.latestCachedRevision.content
            );
        } catch (e) {
            Log.error(e);
            try {
                // Use the API to get the fallback configuration.
                preloadedData = await fetch(APP_FALLBACK_CONFIG, {
                    mode: "no-cors",
                }).then((req) => req.json());
            } catch (e) {
                if (e.message.includes("NetworkError")) {
                    new UltravioletUI.AlertDialog({
                        content: MediaWikiNotificationContent(
                            i18next.t("mediawiki:error.wikiConfigBlocked")
                        ),
                        actions: [{ data: `${i18next.t("ui:close")}` }],
                    });
                }
                // TODO: Proper errors
                throw new AggregateError(
                    "Failed to get on-wiki configuration file.",
                    e
                );
            }
        }
        return preloadedData;
    }

    /**
     * Upgrades a raw configuration if outdated. No-op if already up to date.
     *
     * @param rawConfig The raw configuration to upgrade
     * @return The upgraded configuration
     */
    static async upgradeRawConfiguration(
        rawConfig: Record<string, any>
    ): Promise<WikiConfigurationRaw> {
        /**
         * A fully-upgraded {@link WikiConfigurationRaw} which can then be deserialized into
         * a proper {@link WikiConfiguration}.
         */
        let config: WikiConfigurationRaw;
        if (rawConfig.configVersion < APP_WIKI_CONFIGURATION_VERSION)
            config =
                await UltravioletWikiConfiguration.upgradeWikiConfiguration(
                    rawConfig
                );
        else config = rawConfig as WikiConfigurationRaw;

        return config;
    }

    /**
     * Loads the onwiki configuration file. If a configuration file was not found,
     * it will fall back to the primary Ultraviolet configuration file, located on the
     * English Wikipedia (https://w.wiki/57Wn).
     */
    static async loadWikiConfiguration(): Promise<void> {
        Log.debug("Loading per-wiki configuration...");
        /**
         * A basic JSON object holding keys for what is supposed to be a {@link WikiConfigurationRaw}.
         */
        const rawConfig: Record<string, any> =
            JSON.parse(localStorage.getItem("uv-wikiConfiguration")) ??
            (await UltravioletWikiConfiguration.loadWikiConfigurationFile());
        const config =
            await UltravioletWikiConfiguration.upgradeRawConfiguration(
                rawConfig
            );

        localStorage.setItem("uv-wikiConfiguration", JSON.stringify(config));

        if (config.wiki != mw.config.get("wgDBname")) {
            // No need for i18n; this is debug information.
            Log.warn(
                `External wiki configuration file loaded. Expecting "${
                    config.wiki
                }", got "${mw.config.get(
                    "wgDBname"
                )}" instead. Templates may be missing or broken.`
            );

            // Force remove tag (to avoid errors)
            delete config.meta.tag;
        } else if (rawConfig.configVersion < APP_WIKI_CONFIGURATION_VERSION) {
            // TODO: Suggest saving the upgraded config file to the user (if it's the same wiki).
        }

        UltravioletWikiConfiguration._loadedConfiguration =
            UltravioletWikiConfiguration.deserializeWikiConfiguration(config);

        // Refresh whatever needs refreshing.
        WarningManager.refresh();
        Log.debug("Loaded per-wiki configuration.");

        UltravioletHooks.addHook("postUIInject", async () => {
            Log.debug("Refreshing wiki configuration...");
            const rawConfig =
                await UltravioletWikiConfiguration.loadWikiConfigurationFile();
            const upgradeConfig =
                await UltravioletWikiConfiguration.upgradeRawConfiguration(
                    rawConfig
                );

            if (
                localStorage.getItem("uv-wikiConfiguration") !==
                JSON.stringify(upgradeConfig)
            ) {
                Log.debug("Changes found in per-wiki configuration. Saving...");
                localStorage.setItem(
                    "uv-wikiConfiguration",
                    JSON.stringify(upgradeConfig)
                );
                UVUIToast.quickShow({
                    content: `${i18next.t("misc:config.updated")}`,
                    action: {
                        text: `${i18next.t("ui:reload")}`,
                        callback: () => location.reload(),
                    },
                });
            } else {
                Log.debug("Per-wiki configuration is up to date.");
            }
        });
    }

    /**
     * Attempt to upgrade an outdated configuration file.
     */
    private static async upgradeWikiConfiguration(
        config: Record<string, any>
    ): Promise<WikiConfigurationRaw> {
        return updateWikiConfiguration(config);
    }

    private static deserializeWikiConfiguration(
        config: WikiConfigurationRaw
    ): WikiConfiguration {
        const preDeserializeConfig = JSON.parse(JSON.stringify(config));
        const deserializeChunk = (
            chunk: Record<string, any>,
            deserializerSet: Record<string, any>
        ) => {
            // Run through fields first.
            for (const [key, deserializer] of Object.entries(deserializerSet)) {
                if (chunk[key] != null) {
                    if (typeof deserializer === "function")
                        chunk[key] = deserializer(
                            chunk[key],
                            preDeserializeConfig,
                            config
                        );
                    else if (typeof deserializer._self === "function")
                        chunk[key] = deserializer._self(
                            chunk[key],
                            preDeserializeConfig,
                            config
                        );
                }
            }

            // Run through subrecords.
            for (const [key, value] of Object.entries(deserializerSet)) {
                if (chunk[key] != null && typeof value === "object") {
                    chunk[key] = deserializeChunk(chunk[key], value);
                }
            }

            return chunk;
        };
        return deserializeChunk(
            config,
            WikiConfigurationDeserializers
        ) as WikiConfiguration;
    }
}
