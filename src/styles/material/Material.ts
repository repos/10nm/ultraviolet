import { MDCDialog } from "@material/dialog";
import { MDCRipple } from "@material/ripple";
import { UVUIDialog } from "app/ui/elements/UVUIDialog";
import Style from "app/styles/Style";
import MaterialPreInitializationHooks from "./hooks/MaterialPreInitializationHooks";
import {
    getMaterialStorage,
    MaterialStyleStorage,
} from "./data/MaterialStyleStorage";
import MaterialAlertDialog from "./ui/MaterialAlertDialog";
import MaterialInputDialog from "./ui/MaterialInputDialog";
import MaterialSelectionDialog from "./ui/MaterialSelectionDialog";
import MaterialWarnDialog from "app/styles/material/ui/MaterialWarnDialog";
import MaterialToast from "./ui/MaterialToast";
import MaterialDiffIcons from "./ui/MaterialDiffIcons";
import MaterialIFrameDialog from "app/styles/material/ui/MaterialIFrameDialog";

import "./css/globals.css";
import MaterialPageIcons from "app/styles/material/ui/MaterialPageIcons";
import MaterialExtendedOptions from "app/styles/material/ui/MaterialExtendedOptions";
import MaterialProtectionRequestDialog from "app/styles/material/ui/MaterialProtectionRequestDialog";
import MaterialReportingDialog from "app/styles/material/ui/MaterialReportingDialog";
import promiseSplit from "app/util/promiseSplit";
import MaterialPreferences from "./ui/MaterialPreferences";
import MaterialPreferencesTab from "./ui/MaterialPreferencesTab";
import MaterialPreferencesItem from "./ui/MaterialPreferencesItem";

const MaterialStyle: Style = {
    name: "material",
    version: "1.0.0",

    meta: {
        "en-US": {
            displayName: "Material",
            author: ["The Ultraviolet Development Team (10nm)", "Google, Inc."],
            // \u2014 is an emdash
            description:
                "Ultraviolet's default theme \u2014 the classic look-and-feel of RedWarn with several improvements. This is an implementation of Google's Material Design.",

            homepage: "https://en.wikipedia.org/wiki/WP:UV",
            repository: "https://gitlab.wikimedia.org/repos/10nm/ultraviolet",
            issues: "https://phabricator.wikimedia.org/project/board/6210/",
        },
    },
    dependencies: [
        {
            type: "style",
            id: "mdc-styles-13",
            src: "https://tools-static.wmflabs.org/cdnjs/ajax/libs/material-components-web/13.0.0/material-components-web.min.css",
            cache: {
                delayedReload: true,
                duration: 1209600000, // 14 days
            },
        },
        {
            type: "style",
            id: "roboto",
            src: "https://tools-static.wmflabs.org/fontcdn/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin,latin-ext,vietnamese",
            cache: {
                delayedReload: true,
                duration: 1209600000, // 14 days
            },
        },
    ],

    storage: new MaterialStyleStorage(),

    classMap: {
        uvAlertDialog: MaterialAlertDialog,
        uvInputDialog: MaterialInputDialog,
        uvSelectionDialog: MaterialSelectionDialog,
        uvWarnDialog: MaterialWarnDialog,
        uvProtectionRequestDialog: MaterialProtectionRequestDialog,
        uvIFrameDialog: MaterialIFrameDialog,
        uvToast: MaterialToast,
        uvDiffIcons: MaterialDiffIcons,
        uvPageIcons: MaterialPageIcons,
        uvExtendedOptions: MaterialExtendedOptions,
        uvReportingDialog: MaterialReportingDialog,
        uvPreferences: MaterialPreferences,
        uvPreferencesTab: MaterialPreferencesTab,
        uvPreferencesItem: MaterialPreferencesItem,
    },

    hooks: {
        preInit: [MaterialPreInitializationHooks],
    },
};

export default MaterialStyle;

export function upgradeMaterialDialogButtons(dialog: UVUIDialog<any>): void {
    dialog.element
        .querySelectorAll("button")
        .forEach((el) => new MDCRipple(el).initialize());
}

export interface MaterialDialogInitializationOptions<T> {
    onPostInit?: (mdcDialog: MDCDialog) => PromiseOrNot<void>;
    onClose?: (
        event: Event & { detail: { action: string } }
    ) => PromiseOrNot<T>;
}

export type UpgradedMaterialDialog<T> = MDCDialog & {
    wait: () => Promise<T>;
};

export async function upgradeMaterialDialog<T>(
    dialog: UVUIDialog<any>,
    options?: MaterialDialogInitializationOptions<T>
): Promise<UpgradedMaterialDialog<T>> {
    const styleStorage = getMaterialStorage();
    registerMaterialDialog(dialog);

    upgradeMaterialDialogButtons(dialog);

    const mdcDialog = new MDCDialog(dialog.element);

    const [closePromise, closePromiseResolver] =
        promiseSplit<typeof dialog.result>();
    mdcDialog.listen(
        "MDCDialog:closed",
        async (event: Event & { detail: { action: string } }) => {
            if (options?.onClose) dialog.result = await options.onClose(event);

            styleStorage.dialogTracker.delete(dialog.id);
            closePromiseResolver(dialog.result ?? null);
        }
    );

    mdcDialog.initialize();
    mdcDialog.open();

    if (options?.onPostInit) await options.onPostInit(mdcDialog);

    dialog.dialog = Object.assign(mdcDialog, {
        wait: () => closePromise,
    });
    return dialog.dialog;
}

export function registerMaterialDialog(dialog: UVUIDialog<any>): void {
    getMaterialStorage().dialogTracker.set(dialog.id, dialog);
    document.body.appendChild(dialog.render());
}
