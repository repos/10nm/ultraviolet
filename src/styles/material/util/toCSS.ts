/**
 * @deprecated `style` attribute support has been fixed as of @10nm/tsx-dom@1.4.0. This function is obsolete.
 */
export default function (rules: Record<string, any>): string {
    if (rules == null) return "";

    const transformedRules: Record<string, any> = {};

    for (const [key, value] of Object.entries(rules)) {
        if (value == null) continue;
        const splitKey = key.split(/(?=[A-Z])/).map((v) => v.toLowerCase());
        transformedRules[splitKey.join("-")] = value;
    }

    let compiledRules = "";
    for (const [key, value] of Object.entries(transformedRules)) {
        compiledRules += `${key}:${value};`;
    }

    return compiledRules;
}
