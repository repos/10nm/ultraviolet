import { BaseProps, h } from "@10nm/tsx-dom";
import Sortable from "sortablejs";
import classMix from "app/styles/material/util/classMix";

export function SortableList(
    props: BaseProps & {
        class?: string;
        draggable?: string;
        draggableHandle?: string;
    }
): JSX.Element {
    const el = (
        <ul
            {...Object.fromEntries(
                Object.entries(props).filter(
                    ([k]) => !["children", "class"].includes(k)
                )
            )}
            class={classMix("uv-sortable", props.class)}
        >
            {props.children}
        </ul>
    );

    Sortable.create(el as HTMLElement, {
        animation: 100,
        easing: "cubic-bezier(1, 0, 0, 1)",
        forceFallback: true,
        fallbackClass: "uv-sortable-drag",
        onStart() {
            Sortable.ghost.style.opacity = "0";
        },

        handle: props.draggableHandle,
        draggable: props.draggable ?? "li",
        dataIdAttr: "data-id",
    });

    return el;
}
