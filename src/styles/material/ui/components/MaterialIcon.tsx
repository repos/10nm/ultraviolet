import { BaseProps, h } from "@10nm/tsx-dom";
import expandDataAttributes from "app/styles/material/util/expandDataAttributes";
import classMix from "app/styles/material/util/classMix";

export interface MaterialIconButtonProperties extends BaseProps {
    icon: string;
    iconColor?: string;
    class?: string;
}

export default function (props: MaterialIconButtonProperties): JSX.Element {
    const { icon, iconColor } = props;
    return (
        <span
            class={classMix("material-icons", props.class)}
            style={`color:${iconColor ?? "initial"};`}
            {...expandDataAttributes(props)}
        >
            {icon}
        </span>
    );
}
