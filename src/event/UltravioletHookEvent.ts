export type UltravioletHookEvent =
    | UltravioletLoadEvent
    | UltravioletPreInitializationEvent
    | UltravioletInitializationEvent
    | UltravioletPostInitializationEvent
    | UltravioletPreUIInjectionEvent
    | UltravioletPostUIInjectionEvent
    | UltravioletDeinitializationEvent
    | UltravioletTerminationEvent;

interface UltravioletHookEventBase {
    type: string;
    payload?: Record<string, any>;
}

export interface UltravioletLoadEvent extends UltravioletHookEventBase {
    type: "load";
    payload: undefined;
}

export interface UltravioletPreInitializationEvent
    extends UltravioletHookEventBase {
    type: "preInit";
    payload: undefined;
}

export interface UltravioletInitializationEvent
    extends UltravioletHookEventBase {
    type: "init";
    payload: undefined;
}

export interface UltravioletPostInitializationEvent
    extends UltravioletHookEventBase {
    type: "postInit";
    payload: undefined;
}

export interface UltravioletPreUIInjectionEvent
    extends UltravioletHookEventBase {
    type: "preUIInject";
    payload: undefined;
}

export interface UltravioletPostUIInjectionEvent
    extends UltravioletHookEventBase {
    type: "postUIInject";
    payload: undefined;
}

export interface UltravioletDeinitializationEvent
    extends UltravioletHookEventBase {
    type: "deinit";
    payload: undefined;
}

export interface UltravioletTerminationEvent extends UltravioletHookEventBase {
    type: "terminate";
    payload: undefined;
}

export type UltravioletHookEventTypes = UltravioletHookEvent["type"];
export type UltravioletHook = (
    payload: Record<string, any>
) => Promise<void> | void;
export type UltravioletHookTyped<T extends UltravioletHookEvent> = (
    payload: (UltravioletHook & { payload: T["payload"] })["payload"]
) => Promise<void> | void;
