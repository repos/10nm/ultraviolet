/**
 * An event handler that stops propagation and cancels the event.
 * @param e
 */
export default (e: Event) => {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
};
