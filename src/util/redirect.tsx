import { h } from "@10nm/tsx-dom";

/**
 * Redirects a user to another page.
 * @param url The new page's URL
 * @param inNewTab Whether to open the page in a new tab or not.
 */
export default function (url: string, inNewTab = false): void {
    if (inNewTab) {
        ((<a target="_blank" href={url} />) as HTMLElement).click(); // Open in new tab
    } else {
        window.location.href = url; // open here
    }
}
