export interface Injector {
    init(): PromiseOrNot<void>;
}
