import UVUIElement from "app/ui/elements/UVUIElement";
import {
    DiffIconRevertContext,
    RestoreStage,
    RevertStage,
    Revision,
} from "app/mediawiki";
import { BaseProps } from "@10nm/tsx-dom";
import UVErrorBase from "app/errors/UVError";

export type UVUIDiffIconsProperties = Pick<
    DiffIconRevertContext,
    "oldRevision" | "newRevision" | "latestRevision"
> & {
    side: "old" | "new";
};

/**
 * The UVUIDiffIcons are icons that are displayed on a diff page. Since they are injected
 * into the DOM upon loading, these icons are wrapped by a container, regardless of whether
 * or not the element already provides one. This container is accessible with the ID
 * `#uvDiffIcons`.
 */
export class UVUIDiffIcons
    extends UVUIElement
    implements UVUIDiffIconsProperties
{
    public static readonly elementName = "uvDiffIcons";

    oldRevision: Revision;
    newRevision: Revision;
    latestRevision: Revision;
    side: "old" | "new";
    /**
     * This element, as returned by {@link UVUIDiffIcons.render}.
     */
    self: HTMLElement;

    constructor(props: UVUIDiffIconsProperties & BaseProps) {
        super();
        Object.assign(props, this);
    }

    get isLatestIcons(): boolean {
        return (
            this.latestRevision.revisionID ===
            (this.side === "new"
                ? this.newRevision.revisionID
                : this.oldRevision.revisionID)
        );
    }

    /**
     * Renders the diff icons. These are then wrapped by an Ultraviolet container
     * element to isolate other elements.
     *
     * This is called only once: on insertion. Any subsequent expected changes
     * to this element will be called through other functions.
     */
    render(): JSX.Element {
        return undefined;
    }

    /**
     * Called on the start of a revert.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onStartRevert(context: DiffIconRevertContext): void {
        return undefined;
    }

    /**
     * Called when the stage of a revert has advanced.
     * @param stage The current stage of the revert.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onRevertStageChange(stage: RevertStage): void {
        return undefined;
    }

    /**
     * Called at the end of a revert. This is not called when a revert fails,
     * use {@link onRevertFailure} instead.
     *
     * @param cancelled Whether or not this revert ended due to being cancelled.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onEndRevert(cancelled = false): void {
        // TODO: run post-revert action
        return undefined;
    }

    /**
     * Called when a revert fails with an error.
     * @param error The error that occurred.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onRevertFailure(error: UVErrorBase): void {
        return undefined;
    }

    /**
     * Called on the beginning of a revert/restore. Informs the element of the
     * target revision to revert to.
     * @param targetRevision The revision being reverted to.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onStartRestore(targetRevision: Revision): void {
        return undefined;
    }

    /**
     * Called when the stage of a revert/restore has advanced.
     * @param stage The {@link RestoreStage} of the revert/restore.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onRestoreStageChange(stage: RestoreStage): void {
        return undefined;
    }

    /**
     * Called when a revert/restore has ended with no issues.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onEndRestore(editResponse: { edit: Record<string, any> }): void {
        return undefined;
    }

    /**
     * Called when an error occurrs during a revert/restore.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onRestoreFailure(error: UVErrorBase): void {
        return undefined;
    }
}
