import { h } from "@10nm/tsx-dom";

export default (html: string): HTMLElement => {
    const el = (<span />) as HTMLElement;
    el.innerHTML = html;
    return el;
};
